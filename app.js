// Теоретичні питання:
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування дозволяє одному об'єкту базуватися на основі іншого і розділяти властивості, так як кожний об'єкт має внутрішнє посилання на інший об'єкт.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Конструктор класу-нащадка викликає super(), щоб викликати конструктор класу-батька з зазначеними аргументами.

// Завдання:
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const workerJack = new Programmer('Jack', 30, 2000, 'english');
console.log(workerJack);
console.log(workerJack.salary);

const workerJohn = new Programmer('John', 35, 4000, 'german');
console.log(workerJohn);
console.log(workerJohn.salary);